using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo_3 : MonoBehaviour
{
    public float velocidad = -.2f;
    int Distancia = 15;
    public float X ;

    // Start is called before the first frame update
    void Start()
    {
        X = (transform.localEulerAngles.z - 360);
    }

    // Update is called once per frame
    void Update()
    {
        //Movimiento 
        transform.Rotate(0, 0, velocidad);

        //Cosa rara para sacar el valor en z de la rotaci�n
        float Rotation;
        if (transform.localEulerAngles.z <= 180f)
        {
            Rotation = transform.localEulerAngles.z;
        }
        else
        {
            Rotation = transform.localEulerAngles.z - 360f;
        }
       
        
        //Loop.
        if (Rotation < -50)
        {
           velocidad = -velocidad;
        }

        if (Rotation > 5)
        {
           velocidad = -velocidad;
        }

    }

    void OnMouseEnter()
    {
        velocidad = 1;
    }

    void OnMouseExit()
    {
        velocidad = -.2f;
    }
}
