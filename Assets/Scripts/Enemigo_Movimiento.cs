using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo_Movimiento : MonoBehaviour
{
    public float velocidad = .1f;
    int Distancia = 15;
    public float X;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        X= transform.position.x ;
        
        transform.Translate(velocidad, 0,0);

        if ( X > Distancia) 
        {
            transform.rotation = Quaternion.Euler(0, -180, 0);
        } 
        if (X < -Distancia)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
            
    }

    void OnMouseEnter()
    {
        velocidad = 2;
    }

    void OnMouseExit()
    {
        velocidad = .1f;
    }

}
