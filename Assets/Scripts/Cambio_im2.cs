using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Cambio_im2 : MonoBehaviour
{
    public int seleccion = 1;

    public GameObject pantalla;

    public Sprite insultos1;
    public Sprite insultos2;
    public Sprite insultos3;

    public Button mensajes;
    public Button continuar;

    // Start is called before the first frame update
    void Start()
    {
        mensajes.gameObject.SetActive(true);
        continuar.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (seleccion == 2)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = insultos2;
        }
        else if (seleccion == 3)
        {
            continuar.gameObject.SetActive(true);
            mensajes.gameObject.SetActive(false);
            pantalla.GetComponent<SpriteRenderer>().sprite = insultos3;
        }
        else if (seleccion == 4)
        {
            
            SceneManager.LoadScene(8);
        }

    }
    public void Cambia()
    {
        seleccion++;
    }
}
