using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cambio_Escena : MonoBehaviour
{
    public Texture2D maus;
    public Texture2D mausClik;

    public void Start()
    {
        Cursor.SetCursor(maus, Vector2.zero, CursorMode.ForceSoftware);
    }

    public void OnMouseEnter()
    {

        Debug.Log("holii");
        Cursor.SetCursor(mausClik, Vector2.zero, CursorMode.ForceSoftware);
        
       
    }

    public void OnMouseExit()
    {
        Cursor.SetCursor(maus, Vector2.zero, CursorMode.ForceSoftware);
    }

    public void siguienteEscena()
    {
        PlayerPrefs.SetString("Cambio", "No");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ruta1()
    {
        SceneManager.LoadScene(4);
    }
    public void ruta2()
    {
        SceneManager.LoadScene(5);
    }
    public void ruta3()
    {
        SceneManager.LoadScene(6);
    }
    public void ruta4()
    {
        SceneManager.LoadScene(7);
    }

    public void herramientas()
    {
        SceneManager.LoadScene(8);
    }

    public void amigos()
    {
        SceneManager.LoadScene(9);
    }

    public void enemigos()
    {
        SceneManager.LoadScene(10);
    }

    public void npcs()
    {
        SceneManager.LoadScene(11);
    }

    public void recapitulacion()
    {
        SceneManager.LoadScene(12);
    }

    public void PedirAyuda()
    {
        SceneManager.LoadScene(13);
    }

    public void CallateIdiota()
    {
        SceneManager.LoadScene(14);
    }

    public void Gomene()
    {
        SceneManager.LoadScene(15);
    }

    public void IrtedelaPartida()
    {
        SceneManager.LoadScene(16);
    }

    public void ElFin()
    {
        PlayerPrefs.SetString("Cambio", "Si");
        SceneManager.LoadScene(17);
    }
    public void Home()
    {
        SceneManager.LoadScene(18);
    }
    public void CyberAcosador()
    {
        SceneManager.LoadScene(21);
    }
    public void CyberAcosado()
    {
        SceneManager.LoadScene(20);
    }
    public void Informacion()
    {
        SceneManager.LoadScene(19);
    }
    public void CyberAC_Acosado()
    {
        SceneManager.LoadScene(22);
    }
    public void Observador()
    {
        SceneManager.LoadScene(23);
    }
    public void Minijuegos()
    {
        SceneManager.LoadScene(24);
    }
    public void Shooter()
    {
        SceneManager.LoadScene(25);
    }
    public void Moba()
    {
        SceneManager.LoadScene(26);
    }
    public void HerramientasGrafica()
    {
        SceneManager.LoadScene(27);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
