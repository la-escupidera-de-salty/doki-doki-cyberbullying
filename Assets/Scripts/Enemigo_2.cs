using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo_2 : MonoBehaviour
{
    public float velocidad = .01f;
    public int Distancia = 5;
    public int Distizq;
    public float X;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        X = transform.position.x;

        transform.Translate(velocidad, 0, 0);

        if (X > Distancia)
        {
            transform.rotation = Quaternion.Euler(0, -180, 0);
        }
        if (X < Distizq)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }

    }

    void OnMouseEnter()
    {
        velocidad = 1;
    }

    void OnMouseExit()
    {
        velocidad = .01f;
    }
}
