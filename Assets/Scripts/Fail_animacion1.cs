using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Fail_animacion1 : MonoBehaviour
{
    public int seleccion = 0;
    public Texture2D puntero;
    public Texture2D maus;
    public Texture2D mausClik;
    Animator anim1;
    // Start is called before the first frame update
    void Start()
    {
        anim1 = GetComponent<Animator>();
        Cursor.SetCursor(puntero, Vector2.zero, CursorMode.ForceSoftware);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector2(cursorPos.x, cursorPos.y);

        if (Input.GetMouseButtonDown(0))
        {
            anim1.SetTrigger("Fail");
            seleccion += 1;
        }

        if (seleccion == 10)
        {
            Cursor.SetCursor(maus, Vector2.zero, CursorMode.ForceSoftware);
            SceneManager.LoadScene(2);
        }

    }
}
