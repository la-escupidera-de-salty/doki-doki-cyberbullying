using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Cambio_im5 : MonoBehaviour
{
    public int seleccion = 0;

    public GameObject pantalla;

    public Sprite ejemplos;
    public Sprite acosador;
    public Sprite acosado;
    public Sprite observador;
    public Sprite aCC;
    public Sprite imagenVacia;
    public Button continuar;

    public GameObject botonContinuar;

    public Button amigos;
    public Button enemigos;
    public Button npcs;


    // Start is called before the first frame update
    void Start()
    {
        amigos.gameObject.SetActive(false);
        enemigos.gameObject.SetActive(false);
        npcs.gameObject.SetActive(false);

        continuar.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        // if (Input.GetKeyDown(KeyCode.RightArrow))
        // {
        //     seleccion++;
        // }

        // if (Input.GetKeyDown(KeyCode.LeftArrow))
        // {
        //    seleccion--;
        //}




        if (seleccion == 2)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = ejemplos;
        }
        else if (seleccion == 8)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = acosador;
        }
        else if (seleccion == 12)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = acosado;
        }
        else if (seleccion == 15)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = observador;
        }
        else if (seleccion == 16)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = aCC;
           
        }

        else if (seleccion == 18)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = imagenVacia;
            continuar.gameObject.SetActive(false);
            amigos.gameObject.SetActive(true);
            enemigos.gameObject.SetActive(true);
            npcs.gameObject.SetActive(true);

        }

        /*else if (seleccion == 19)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }*/
    }

    public void desaparece()
    {
        if (seleccion == 18)
        {
            botonContinuar.SetActive(false);
        }
    }
    public void Cambia()
    {
        seleccion++;
    }

}
