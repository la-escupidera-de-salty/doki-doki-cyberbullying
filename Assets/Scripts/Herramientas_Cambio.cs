using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Herramientas_Cambio : MonoBehaviour
{
    public Sprite noHerramientas;
    public Sprite siHerramientas;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        string pantalla = PlayerPrefs.GetString("Cambio");
        if (pantalla == "Si")
        {
            GetComponent<SpriteRenderer>().sprite = siHerramientas;
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = noHerramientas;
        }
        }
}
