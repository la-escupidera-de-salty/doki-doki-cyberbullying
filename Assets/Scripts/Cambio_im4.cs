using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Cambio_im4 : MonoBehaviour
{
    public int seleccion = 0;

    public GameObject pantalla;

    public Sprite ejemplos;
    public Sprite acosador;
    public Sprite acosado;
    public Sprite observador;
    public Sprite aCC;
    public Sprite imagenVacia;

   /* public Button mensajes;
    public Button opcion1;
    public Button opcion2;
    public Button opcion3;
    public Button opcion4; */


    // Start is called before the first frame update
    void Start()
    {
        /*
        mensajes.enabled = true;
        opcion1.enabled = false;
        opcion2.enabled = false;
        opcion3.enabled = false;
        opcion4.enabled = false;

        mensajes.gameObject.SetActive(true);
        opcion1.gameObject.SetActive(false);
        opcion2.gameObject.SetActive(false);
        opcion3.gameObject.SetActive(false);
        opcion4.gameObject.SetActive(false); */
    }

    // Update is called once per frame
    void Update()
    {
        // if (Input.GetKeyDown(KeyCode.RightArrow))
        // {
        //     seleccion++;
        // }

        // if (Input.GetKeyDown(KeyCode.LeftArrow))
        // {
        //    seleccion--;
        //}




        if (seleccion == 3)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = ejemplos;
        }
        else if (seleccion == 5)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = acosador;
        }
        else if (seleccion == 6)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = acosado;
        }
        else if (seleccion == 7)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = observador;
        }
        else if (seleccion == 8)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = aCC;
           /* mensajes.enabled = false;
            opcion1.enabled = true;
            opcion2.enabled = true;
            opcion3.enabled = true;
            opcion4.enabled = true;

            mensajes.gameObject.SetActive(false);
            opcion1.gameObject.SetActive(true);
            opcion2.gameObject.SetActive(true);
            opcion3.gameObject.SetActive(true);
            opcion4.gameObject.SetActive(true); */
        }

        else if (seleccion == 9)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = imagenVacia;
        }

        else if (seleccion == 11)
        {
            SceneManager.LoadScene(18);
        }
    }
    public void Cambia()
    {
        seleccion++;
    }

}
