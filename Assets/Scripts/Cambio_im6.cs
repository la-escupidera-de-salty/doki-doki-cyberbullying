using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Cambio_im6 : MonoBehaviour
{
    public int seleccion = 0;

    public GameObject pantalla;

    public Sprite Barrasvacias;
    public Sprite Barrasllenas;
    public Sprite Fin;
    
    public GameObject botonContinuar;

    public Button salir;
    public Button continuar;


    // Start is called before the first frame update
    void Start()
    {
        continuar.gameObject.SetActive(true);
        salir.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // if (Input.GetKeyDown(KeyCode.RightArrow))
        // {
        //     seleccion++;
        // }

        // if (Input.GetKeyDown(KeyCode.LeftArrow))
        // {
        //    seleccion--;
        //}




        if (seleccion == 1)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = Barrasvacias;
        }
        else if (seleccion == 2)
        {
            pantalla.GetComponent<SpriteRenderer>().sprite = Barrasllenas;
        }
        else if (seleccion == 4)
        {
            SceneManager.LoadScene(18);
            /* pantalla.GetComponent<SpriteRenderer>().sprite = Fin;
             salir.gameObject.SetActive(true);
             continuar.gameObject.SetActive(false); */
        }
        else if (seleccion == 5)
        {
            Application.Quit();
        }
       

                /*else if (seleccion == 19)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }*/
    }

    
    public void Cambia()
    {
        seleccion++;
    }

}
